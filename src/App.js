import React, { Component } from "react";
import Header from "./components/Header";
import Contacts from "./components/Contacts";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header brand="Contact Manager" />
        <div className="container mt-2">
          <Contacts />
        </div>
      </div>
    );
  }
}

export default App;
