import React, { Component } from "react";
import Contact from "./Contact";

class Contacts extends Component {
  constructor() {
    super();
    this.state = {
      contacts: [
        {
          id: 1,
          name: "Jone Doe",
          email: "janedoe@yahoo.com",
          phone: "111-111-1111"
        },
        {
          id: 2,
          name: "William Go",
          email: "wgo@gmail.com",
          phone: "222-222-2222"
        },
        {
          id: 3,
          name: "Fred Johnson",
          email: "fjohnson@yahoo.com",
          phone: "123-456-7899"
        }
      ]
    };
  }

  render() {
    // Desctructuring using curly {} braces
    const { contacts } = this.state;

    return (
      <div>
        {contacts.map(contact => (
          <Contact key={contact.id} contact={contact} />
        ))}
      </div>
    );
  }
}

export default Contacts;
